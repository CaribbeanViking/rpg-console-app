namespace rpg_console_appTest
{
    public class CharacterTests
    {
        [Fact]
        public void Warrior_CreatesNewWarrior_NewWarriorShouldResultInLevelEqualToOne()
        {
            // Arrange
            Warrior warrior = new();
            int expected = 1;
            // Act
            int actual = warrior.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_IncreasesLevelofWarrior_ShouldResultInLevelEqualToTwo()
        {
            // Arrange
            Warrior warrior = new();
            int expected = 2;
            // Act
            warrior.LevelUp();
            int actual = warrior.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void PrimaryAttributes_SetsDefaultPrimaryAttributesToWarriorOnCreation_ShouldResultInCorrectDefaultAttributes()
        {
            // Arrange
            Warrior warrior = new();
            int expectedDexterity = 2;
            int expectedStrength = 5;
            int expectedIntelligence = 1;
            // Act
            int actualDexterity = warrior.PrimaryAttributes.GetDexterity();
            int actualStrength = warrior.PrimaryAttributes.GetStrength();
            int actualIntelligence = warrior.PrimaryAttributes.GetIntelligence();
            // Assert
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        [Fact]
        public void PrimaryAttributes_SetsDefaultPrimaryAttributesToRangerOnCreation_ShouldResultInCorrectDefaultAttributes()
        {
            // Arrange
            Ranger ranger = new();
            int expectedDexterity = 7;
            int expectedStrength = 1;
            int expectedIntelligence = 1;
            // Act
            int actualDexterity = ranger.PrimaryAttributes.GetDexterity();
            int actualStrength = ranger.PrimaryAttributes.GetStrength();
            int actualIntelligence = ranger.PrimaryAttributes.GetIntelligence();
            // Assert
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        [Fact]
        public void PrimaryAttributes_SetsDefaultPrimaryAttributesToMageOnCreation_ShouldResultInCorrectDefaultAttributes()
        {
            // Arrange
            Mage mage = new();
            int expectedDexterity = 1;
            int expectedStrength = 1;
            int expectedIntelligence = 8;
            // Act
            int actualDexterity = mage.PrimaryAttributes.GetDexterity();
            int actualStrength = mage.PrimaryAttributes.GetStrength();
            int actualIntelligence = mage.PrimaryAttributes.GetIntelligence();
            // Assert
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        [Fact]
        public void PrimaryAttributes_SetsDefaultPrimaryAttributesToRogueOnCreation_ShouldResultInCorrectDefaultAttributes()
        {
            // Arrange
            Rogue rogue = new();
            int expectedDexterity = 6;
            int expectedStrength = 2;
            int expectedIntelligence = 1;
            // Act
            int actualDexterity = rogue.PrimaryAttributes.GetDexterity();
            int actualStrength = rogue.PrimaryAttributes.GetStrength();
            int actualIntelligence = rogue.PrimaryAttributes.GetIntelligence();
            // Assert
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        [Fact]
        public void LevelUpAndPrimaryAttributes_IncreasesLevelofWarriorAndSetsIncreasedPrimaryAttributes_ShouldResultInCorrectLeveledUpAttributes()
        {
            // Arrange
            Warrior warrior = new();
            int expectedDexterity = 4;
            int expectedStrength = 8;
            int expectedIntelligence = 2;
            // Act
            warrior.LevelUp();
            int actualDexterity = warrior.PrimaryAttributes.GetDexterity();
            int actualStrength = warrior.PrimaryAttributes.GetStrength();
            int actualIntelligence = warrior.PrimaryAttributes.GetIntelligence();
            // Assert
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        [Fact]
        public void LevelUpAndPrimaryAttributes_IncreasesLevelofMageAndSetsIncreasedPrimaryAttributes_ShouldResultInCorrectLeveledUpAttributes()
        {
            // Arrange
            Mage mage = new();
            int expectedDexterity = 2;
            int expectedStrength = 2;
            int expectedIntelligence = 13;
            // Act
            mage.LevelUp();
            int actualDexterity = mage.PrimaryAttributes.GetDexterity();
            int actualStrength = mage.PrimaryAttributes.GetStrength();
            int actualIntelligence = mage.PrimaryAttributes.GetIntelligence();
            // Assert
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        [Fact]
        public void LevelUpAndPrimaryAttributes_IncreasesLevelofRangerAndSetsIncreasedPrimaryAttributes_ShouldResultInCorrectLeveledUpAttributes()
        {
            // Arrange
            Ranger ranger = new();
            int expectedDexterity = 12;
            int expectedStrength = 2;
            int expectedIntelligence = 2;
            // Act
            ranger.LevelUp();
            int actualDexterity = ranger.PrimaryAttributes.GetDexterity();
            int actualStrength = ranger.PrimaryAttributes.GetStrength();
            int actualIntelligence = ranger.PrimaryAttributes.GetIntelligence();
            // Assert
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }

        [Fact]
        public void LevelUpAndPrimaryAttributes_IncreasesLevelofRogueAndSetsIncreasedPrimaryAttributes_ShouldResultInCorrectLeveledUpAttributes()
        {
            // Arrange
            Rogue rogue = new();
            int expectedDexterity = 10;
            int expectedStrength = 3;
            int expectedIntelligence = 2;
            // Act
            rogue.LevelUp();
            int actualDexterity = rogue.PrimaryAttributes.GetDexterity();
            int actualStrength = rogue.PrimaryAttributes.GetStrength();
            int actualIntelligence = rogue.PrimaryAttributes.GetIntelligence();
            // Assert
            Assert.Equal(expectedDexterity, actualDexterity);
            Assert.Equal(expectedStrength, actualStrength);
            Assert.Equal(expectedIntelligence, actualIntelligence);
        }
    }
}