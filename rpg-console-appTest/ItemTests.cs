﻿using rpg_console_app.Exceptions;

namespace rpg_console_appTest
{
    public class ItemTests
    {
        [Fact]
        public void EquipWeapon_EquipsWarriorWithTooHighLevelWeapon_ShouldThrowAWeaponLevelException()
        {
            // Arrange
            Warrior warrior = new();
            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon("hammer"));
        }

        [Fact]
        public void EquipArmour_EquipsWarriorWithTooHighLevelArmour_ShouldThrowAnArmourLevelException()
        {
            // Arrange
            Warrior warrior = new();
            // Act & Assert
            Assert.Throws<InvalidArmourException>(() => warrior.EquipArmour("body", "mail"));
        }

        [Fact]
        public void EquipWeapon_EquipsWarriorWithIncompatibleWeaponType_ShouldThrowAWeaponIncompatabilityException()
        {
            // Arrange
            Warrior warrior = new();
            // Act & Assert
            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon("bow"));
        }

        [Fact]
        public void EquipArmour_EquipsWarriorWithIncompatibleArmourType_ShouldThrowAnArmourIncompatabilityException()
        {
            // Arrange
            Warrior warrior = new();
            // Act & Assert
            Assert.Throws<InvalidArmourException>(() => warrior.EquipArmour("leg", "cloth"));
        }

        [Fact]
        public void EquipWeapon_EquipsWarriorWithNewWeapon_ShouldReturnSuccessMessage()
        {
            // Arrange
            Warrior warrior = new();
            string expected = "New weapon equipped! (axe)";
            // Act
            string actual = warrior.EquipWeapon("axe");
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipArmour_EquipsWarriorWithNewArmour_ShouldReturnSuccessMessage()
        {
            // Arrange
            Warrior warrior = new();
            string expected = "New armour equipped! (body) (plate)";
            // Act
            string actual = warrior.EquipArmour("body", "plate");
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateHeroDamage_CalculatesHeroDamageWithNoWeapon_ShouldReturnCalculatedHeroDamage()
        {
            // Arrange
            Warrior warrior = new();
            double expected = 1.05;
            // Act
            double actual = warrior.Damage;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateHeroDamage_CalculatesHeroDamageWithAxe_ShouldReturnCalculatedHeroDamage()
        {
            // Arrange
            Warrior warrior = new();
            warrior.EquipWeapon("axe");
            double expected = 8.085;
            // Act
            double actual = warrior.Damage;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateHeroDamage_CalculatesHeroDamageWithAxeAndPlateBodyArmour_ShouldReturnCalculatedHeroDamage()
        {
            // Arrange
            Warrior warrior = new();
            warrior.EquipWeapon("axe");
            warrior.EquipArmour("body", "plate");
            double expected = 8.162;
            // Act
            double actual = warrior.Damage;
            // Assert
            Assert.Equal(expected, actual);
        }
    }
}