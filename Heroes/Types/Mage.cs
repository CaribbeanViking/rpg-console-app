﻿using rpg_console_app.Hero;
using rpg_console_app.Items;
/// <summary>
/// Mage Class. Contains constants and constructor needed to create a new Mage.
/// Inherits from Hero base CLass.
/// </summary>
public class Mage : Hero
{
    /// <summary>
    /// Constants
    /// </summary>
    private const string TYPE = "Mage";

    private const int INIT_DEXTERITY = 1;
    private const int INIT_STRENGTH = 1;
    private const int INIT_INTELLIGENCE = 8;
    private const int INIT_LEVEL = 1;

    private const int LEVEL_UP_DEXTERITY_RATE = 1;
    private const int LEVEL_UP_STRENGTH_RATE = 1;
    private const int LEVEL_UP_INTELLIGENCE_RATE = 5;

    private const double INIT_DAMAGE = 1 + (double)INIT_INTELLIGENCE / 100;

    List<string> ALLOWED_WEAPONS = new List<string> { "staff", "wand" };
    List<string> ALLOWED_ARMOUR = new List<string> { "cloth" };

    /// <summary>
    /// Mage constructor. Constructs a new Mage with properties from constants.
    /// </summary>
    public Mage()
    {
        Type = TYPE;
        Level = INIT_LEVEL;
        Damage = INIT_DAMAGE;
        PrimaryAttributes = new(INIT_DEXTERITY, INIT_STRENGTH, INIT_INTELLIGENCE);
        SecondaryAttributes = new(0, 0, 0); // Zeros when no equipped Armour.
        LevelUpRate = new(LEVEL_UP_DEXTERITY_RATE, LEVEL_UP_STRENGTH_RATE, LEVEL_UP_INTELLIGENCE_RATE);

        foreach (string weapon in ALLOWED_WEAPONS) // Add allowed weapons to Mage.
        {
            allowedWeapons?.Add(weapon);
        }
        foreach (string armour in ALLOWED_ARMOUR) // Add allowed armour to Mage.
        {
            allowedArmour?.Add(armour);
        }
        Console.WriteLine("A " + TYPE + " has spawned!");
    }
    /// <summary>
    /// Overriden function from Hero base class. Sets Secondary attributes according to Hero's damage parameter.
    /// </summary>
    public override void AddAttributes()
    {
        SecondaryAttributes.Intelligence = 0;
        foreach (Armour armour in ArmourArray) // Add up attributes
        {
            if (armour != null)
            {
                SecondaryAttributes.Intelligence += armour.Intelligence;
            }
        }
    }

    /// <summary>
    /// Overriden function from Hero base class. Sets hero damage according to Hero's damage parameter.
    /// </summary>
    public override void CalculateHeroDamage()
    {
        double damagePerSecond;
        if (WeaponEquiped == null)
        {
            damagePerSecond = 1; // Set Damage Per Second to 1 if no weapon equipped
        }
        else
        {
            damagePerSecond = WeaponEquiped.AttackSpeed * WeaponEquiped.Damage;
        }
        AddAttributes(); // Call function to add upp current second attributes
        int totalAttributes = PrimaryAttributes.Intelligence + SecondaryAttributes.Intelligence;
        Damage = Math.Round(damagePerSecond * (1 + ((double)totalAttributes / 100)), 3); // Set Damage according to formula and round to 3 figures
    }
}
