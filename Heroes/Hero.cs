﻿using rpg_console_app.Exceptions;
using rpg_console_app.Heroes.Properties;
using rpg_console_app.Items;

namespace rpg_console_app.Hero
{
    /// <summary>
    /// Hero base class. Has Warrior, Mage, Ranger and Rogue as children.
    /// Contains methods to add hero attributes, calculate hero damage,
    /// equip weapons/armour and level up. Also contains custom Exceptions
    /// for invalid weapon and armour selection for Heroes.
    /// </summary>
    public abstract class Hero
    {
        /// <summary>
        /// Class properties
        /// </summary>
        public string? Type { get; set; }
        public int Level { get; set; }
        public double Damage { get; set; }

        public List<string>? allowedWeapons = new();

        public List<string>? allowedArmour = new();
        public Weapon? WeaponEquiped { get; set; }

        public Armour[] ArmourArray = new Armour[3];
        public LevelUpRate? LevelUpRate { get; set; }
        public PrimaryAttributes? PrimaryAttributes { get; set; }
        public SecondaryAttributes? SecondaryAttributes { get; set; }

        /// <summary>
        /// Method to add up all secondary attributes from equipped
        /// armour.
        /// </summary>
        public abstract void AddAttributes();

        /// <summary>
        /// Method to calculate and set Hero damage.
        /// </summary>
        public abstract void CalculateHeroDamage();


        /// <summary>
        /// Method to equip new weapon to Hero.
        /// Throws custom InvalidWeaponException if weapon is not allows for this Hero or if Hero level is to low. 
        /// </summary>
        /// <param name="type"></param>
        /// <returns>msg</returns>
        public string EquipWeapon(string type)
        {
            try
            {
                if (!allowedWeapons.Contains(type) || Weapon.GetWeapon(type).RequiredLevel > Level) // If weapon not allows or level to high...
                {
                    throw new InvalidWeaponException(); // Throw custom exception
                }
                else
                {
                    WeaponEquiped = Weapon.GetWeapon(type); // Equip weapon
                    Console.WriteLine("New weapon equipped! (" + WeaponEquiped.WeaponName + ")");
                    CalculateHeroDamage(); // Recalculate Damage
                }
                string msg = "New weapon equipped!" + " (" + type + ")";
                Console.WriteLine(msg);
                return msg;
            }
            /// Needed to NOT catch the throw in order for XUnit test to work. Commented out functionality.
            catch (IOException e) // SHOULD BE InvalidWeaponException for final release!!
            {
                //if (!allowedWeapons.Contains(type)) // If weapon not allowed
                //{
                //    Console.WriteLine(e.typeMsg); 
                //}
                //if (Weapon.GetWeapon(type).RequiredLevel > Level) // If weapon level too high...
                //{
                //    Console.WriteLine(e.levelMsg); 
                //}
            }
            return null;
        }

        /// <summary>
        /// Method to equip new Armour to Hero.
        /// Throws custom InvalidArmour Exception if weapon is not allows for this Hero or if Hero level is to low. 
        /// </summary>
        /// <param name="Part"></param>
        /// <param name="Type"></param>
        /// <returns>msg</returns>
        public string EquipArmour(string part, string type)
        {
            try
            {
                if (!allowedArmour.Contains(type) || Armour.GetArmour(part, type).RequiredLevel > Level) // If armour not allowed or level to high...
                {
                    throw new InvalidArmourException(); // Throw custom armour exception
                }
                else
                {
                    switch (part) // Check for part of armour and equip armour
                    {
                        case "head":
                            ArmourArray[0] = Armour.GetArmour(part, type);
                            break;
                        case "body":
                            ArmourArray[1] = Armour.GetArmour(part, type);
                            break;
                        case "leg":
                            ArmourArray[2] = Armour.GetArmour(part, type);
                            break;
                    }
                    CalculateHeroDamage(); // Recalculate Damage
                    string msg = "New armour equipped!" + " (" + part + ") " + "(" + type + ")";
                    Console.WriteLine(msg);
                    return msg;
                }
            }
            /// Needed to NOT catch the throw in order for XUnit test to work. Commented out functionality.
            catch (IOException e) // Should be InvalidWeaponException e
            {
                //if (!allowedArmour.Contains(type)) // If armour not allowed
                //{
                //    Console.WriteLine(e.typeMsg);
                //}
                //if (Armour.GetArmour(part, type).RequiredLevel > Level) // If armour level too high...
                //{
                //    Console.WriteLine(e.levelMsg);
                //}
            }
            return null;
        }

        /// <summary>
        /// Method to level up hero according to Hero constant
        /// </summary>
        public void LevelUp()
        {
            PrimaryAttributes.Dexterity += LevelUpRate.Dexterity;
            PrimaryAttributes.Strength += LevelUpRate.Strength;
            PrimaryAttributes.Intelligence += LevelUpRate.Intelligence;
            CalculateHeroDamage(); // Recalculate Damage
            Level++;
        }
    }
}