﻿namespace rpg_console_app.Heroes.Properties
{
    /// <summary>
    /// SecondaryAttributes Class.
    /// Getters, setters and constructor. Contains a Hero's primary properties.
    /// </summary>
    public class PrimaryAttributes
    {
        /// <summary>
        /// Class properties
        /// </summary>
        internal int Dexterity { get; set; }
        internal int Strength { get; set; }
        internal int Intelligence { get; set; }
        /// <summary>
        /// Constructor setting the instance properties.
        /// </summary>
        /// <param name="Dexterity"></param>
        /// <param name="Strength"></param>
        /// <param name="Intelligence"></param>
        internal PrimaryAttributes(int Dexterity, int Strength, int Intelligence)
        {
            this.Dexterity = Dexterity;
            this.Strength = Strength;
            this.Intelligence = Intelligence;
        }

        /// <summary>
        /// Getters used for easier XUnit testing.
        /// </summary>
        /// <returns></returns>
        public int GetDexterity()
        {
            return Dexterity;
        }
        public int GetStrength()
        {
            return Strength;
        }
        public int GetIntelligence()
        {
            return Intelligence;
        }
    }
}