﻿namespace rpg_console_app.Heroes.Properties
{
    /// <summary>
    /// SecondaryAttributes Class.
    /// Inherites all from Class PrimaryAttributes.
    /// </summary>
    public class SecondaryAttributes : PrimaryAttributes
    {
        /// <summary>
        /// Constructor based on PrimaryAttributes class.
        /// </summary>
        /// <param name="Dexterity"></param>
        /// <param name="Strength"></param>
        /// <param name="Intelligence"></param>
        internal SecondaryAttributes(int Dexterity, int Strength, int Intelligence) : base(Dexterity, Strength, Intelligence)
        {
        }
    }
}