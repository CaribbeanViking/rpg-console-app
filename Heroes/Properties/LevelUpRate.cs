﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_console_app.Heroes.Properties
{
    /// <summary>
    /// LevelUpRate Class.
    /// Getters, setters and constructor. Levels up a Hero according to their properties.
    /// </summary>
    public class LevelUpRate
    {
        /// <summary>
        /// Class properties
        /// </summary>
        internal int Dexterity { get; set; }
        internal int Strength { get; set; }
        internal int Intelligence { get; set; }
        /// <summary>
        /// Constructor setting the instance properties.
        /// </summary>
        /// <param name="Dexterity"></param>
        /// <param name="Strength"></param>
        /// <param name="Intelligence"></param>
        internal LevelUpRate(int Dexterity, int Strength, int Intelligence)
        {
            this.Dexterity = Dexterity;
            this.Strength = Strength;
            this.Intelligence = Intelligence;
        }
    }
}