﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rpg_console_app.Exceptions
{
    /// <summary>
    /// Custom Exception Class. Prints out msg alerting to armour mismatch or Hero level to low.
    /// </summary>
    public class InvalidArmourException : Exception
    {
        public string levelMsg = "Cannot equip this armour! Hero's level is to low!";
        public string typeMsg = "Cannot equip this armour! Armour type is incompatible with this Hero!";
    }
}
