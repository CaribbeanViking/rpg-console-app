## Release
Version: 1.0
Date: 2022-08-08

## Description
This is a Console Application written in C#. It is an RPG game. It allows a user to choose from four different characters which can be leveled up, equipped with armour and weapons.

## Installation

None.

## Using the application

Just run Program.cs to se some manually done tests. XUnit test project is also included for testing of certain functionality.

## Help

Thouroghly commented for assistance.

## Contributors

https://gitlab.com/CaribbeanViking

## Contributing

No one else than above mentioned.

## Known Bugs

Catching the custom error in EquipWeapon and EquipArmour functions are not implemented since catching the errors makes XUnit Test fail. Code for finall project in these functions are therefore commented out.
