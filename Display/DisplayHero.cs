﻿using System.Text;

namespace rpg_console_app.Display
{
    /// <summary>
    /// DisplayHero Class.
    /// Takes a StringBuilder object and creates a display of Hero stats. Prints to console.
    /// </summary>
    internal class DisplayHero
    {
        /// <summary>
        /// Method to print out Hero stats to console.
        /// </summary>
        /// <param name="SB"></param>
        /// <param name="W"></param>
        internal static void DisplayHeroStats(StringBuilder SB, Warrior W)
        {
            SB.AppendLine("Name:         " + W.Type);
            SB.AppendLine("Weapon:       " + W.WeaponEquiped?.WeaponName);
            SB.AppendLine("Armour:       " + W.ArmourArray[0]?.ArmourName + W.ArmourArray[1]?.ArmourName + W.ArmourArray[2]?.ArmourName);
            SB.AppendLine("Level:        " + W.Level.ToString());
            SB.AppendLine("Strength:     " + (W.PrimaryAttributes.Strength + W.SecondaryAttributes.Strength).ToString());
            SB.AppendLine("Dexterity:    " + (W.PrimaryAttributes.Dexterity + W.SecondaryAttributes.Dexterity).ToString());
            SB.AppendLine("Intelligence: " + (W.PrimaryAttributes.Intelligence + W.SecondaryAttributes.Intelligence).ToString());
            SB.AppendLine("Damage:       " + W.Damage.ToString());
            Console.WriteLine(SB);
        }
    }
}