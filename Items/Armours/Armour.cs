﻿using rpg_console_app.Heroes.Properties;

namespace rpg_console_app.Items
{
    /// <summary>
    /// Armour Class.
    /// Includes constants describing several armour properties, constructor and
    /// method to return an Armour object.
    /// </summary>
    public class Armour
    {
        /// <summary>
        /// Constants.
        /// </summary>
        // Slot
        private const string HEAD_ARMOUR_SLOT = "HEAD_ARMOUR_SLOT";
        private const string BODY_ARMOUR_SLOT = "BODY_ARMOUR_SLOT";
        private const string LEG_ARMOUR_SLOT = "LEG_ARMOUR_SLOT";

        // Cloth
        private const int REQUIRED_LEVEL_CLOTH = 1;
        private const int CLOTH_STRENGTH = 2;
        private const int CLOTH_DEXTERITY = 3;
        private const int CLOTH_INTELLIGENCE = 4;

        // Leather
        private const int REQUIRED_LEVEL_LEATHER = 3;
        private const int LEATHER_STRENGTH = 4;
        private const int LEATHER_DEXTERITY = 5;
        private const int LEATHER_INTELLIGENCE = 7;

        // Mail
        private const int REQUIRED_LEVEL_MAIL = 5;
        private const int MAIL_STRENGTH = 5;
        private const int MAIL_DEXTERITY = 6;
        private const int MAIL_INTELLIGENCE = 9;

        // Plate
        private const int REQUIRED_LEVEL_PLATE = 1;
        private const int PLATE_STRENGTH = 1;
        private const int PLATE_DEXTERITY = 8;
        private const int PLATE_INTELLIGENCE = 10;

        /// <summary>
        /// Class properties
        /// </summary>
        internal string ArmourName { get; set; }
        internal int RequiredLevel { get; set; }
        internal string Slot { get; set; }
        internal int Dexterity { get; set; }
        internal int Strength { get; set; }
        internal int Intelligence { get; set; }

        /// <summary>
        /// Constructor setting the instance properties.
        /// </summary>
        /// <param name="ArmourName"></param>
        /// <param name="RequiredLevel"></param>
        /// <param name="Slot"></param>
        /// <param name="Dexterity"></param>
        /// <param name="Strength"></param>
        /// <param name="Intelligence"></param>
        internal Armour(string ArmourName, int RequiredLevel, string Slot, int Dexterity, int Strength, int Intelligence)
        {
            this.ArmourName = ArmourName;
            this.RequiredLevel = RequiredLevel;
            this.Slot = Slot;
            this.Dexterity = Dexterity;
            this.Strength = Strength;
            this.Intelligence = Intelligence;
        }

        /// <summary>
        /// Method to return a new Armour object with corresponding properties.
        /// Throws Exception if Type doesn't match head/body/leg.
        /// </summary>
        /// <param name="Part"></param>
        /// <param name="Type"></param>
        /// <returns>Armour</returns>
        internal static Armour? GetArmour(string Part, string Type)
        {
            try
            {
                return Part switch
                {
                    ("head") => new Armour("Head", GetLevel(Type), HEAD_ARMOUR_SLOT, ArmourAttributes(Type).Dexterity,
                                                ArmourAttributes(Type).Strength, ArmourAttributes(Type).Intelligence),
                    ("body") => new Armour("Body", GetLevel(Type), BODY_ARMOUR_SLOT, ArmourAttributes(Type).Dexterity,
                                                ArmourAttributes(Type).Strength, ArmourAttributes(Type).Intelligence),
                    ("leg") => new Armour("Leg", GetLevel(Type), LEG_ARMOUR_SLOT, ArmourAttributes(Type).Dexterity,
                                                ArmourAttributes(Type).Strength, ArmourAttributes(Type).Intelligence),
                    _ => throw new Exception(),
                };
            }
            catch (Exception)
            {
                Console.WriteLine("Wrong input! Try using only lower case chars.");
                return null;
            }
        }

        /// <summary>
        /// Method to return the required level to equip Armour.
        /// Throws Exception if Type doesn't match cloth/leather/mail/plate.
        /// </summary>
        /// <param name="Type"></param>
        /// <returns>int</returns>
        private static int GetLevel(string Type)
        {
            try
            {
                return Type switch
                {
                    "cloth" => REQUIRED_LEVEL_CLOTH,
                    "leather" => REQUIRED_LEVEL_LEATHER,
                    "mail" => REQUIRED_LEVEL_MAIL,
                    "plate" => REQUIRED_LEVEL_PLATE,
                    _ => throw new Exception(),
                };
            }
            catch (Exception)
            {
                Console.WriteLine("Wrong input! Try using only lower case chars.");
                return 0;
            }
        }

        /// <summary>
        /// Method to return the secondary attributes corresponding to equipped Armour.
        /// Throws Exception if Type doesn't match cloth/leather/mail/plate.
        /// </summary>
        /// <param name="Type"></param>
        /// <returns>SecondaryAttribute</returns>
        private static SecondaryAttributes? ArmourAttributes(string Type)
        {
            try
            {
                return Type switch
                {
                    "cloth" => new SecondaryAttributes(CLOTH_DEXTERITY, CLOTH_STRENGTH, CLOTH_INTELLIGENCE),
                    "leather" => new SecondaryAttributes(LEATHER_DEXTERITY, LEATHER_STRENGTH, LEATHER_INTELLIGENCE),
                    "mail" => new SecondaryAttributes(MAIL_DEXTERITY, MAIL_STRENGTH, MAIL_INTELLIGENCE),
                    "plate" => new SecondaryAttributes(PLATE_DEXTERITY, PLATE_STRENGTH, PLATE_INTELLIGENCE),
                    _ => throw new Exception(),
                };
            }
            catch (Exception)
            {
                Console.WriteLine("Wrong input! Try using only lower case chars.");
                return null;
            }
        }
    }
}
