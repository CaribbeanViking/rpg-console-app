﻿namespace rpg_console_app.Items
{
    /// <summary>
    /// Weapon Class.
    /// Includes constants describing several weapon properties, constructor and
    /// method to return a Weapon object.
    /// </summary>
    public class Weapon
    {
        /// <summary>
        /// Constants.
        /// </summary>
        // Slot
        private const string WEAPON_SLOT = "WEAPON_SLOT";

        // Axe
        private const int REQUIRED_LEVEL_AXE = 1;
        private const double DAMAGE_AXE = 7;
        private const double ATTACKSPEED_AXE = 1.1;

        // Bow
        private const int REQUIRED_LEVEL_BOW = 2;
        private const double DAMAGE_BOW = 18;
        private const double ATTACKSPEED_BOW = 2.1;

        // Dagger
        private const int REQUIRED_LEVEL_DAGGER = 3;
        private const double DAMAGE_DAGGER = 4;
        private const double ATTACKSPEED_DAGGER = 3.0;

        // Hammer
        private const int REQUIRED_LEVEL_HAMMER = 4;
        private const double DAMAGE_HAMMER = 8;
        private const double ATTACKSPEED_HAMMER = 1.5;

        // Staff
        private const int REQUIRED_LEVEL_STAFF = 5;
        private const double DAMAGE_STAFF = 11;
        private const double ATTACKSPEED_STAFF = 0.5;

        // Sword
        private const int REQUIRED_LEVEL_SWORD = 6;
        private const double DAMAGE_SWORD = 20;
        private const double ATTACKSPEED_SWORD = 2.4;

        // Wand
        private const int REQUIRED_LEVEL_WAND = 10;
        private const double DAMAGE_WAND = 7;
        private const double ATTACKSPEED_WAND = 5.1;

        /// <summary>
        /// Class properties
        /// </summary>
        internal string WeaponName { get; set; }
        internal int RequiredLevel { get; set; }
        internal string Slot { get; set; }
        internal double Damage { get; set; }
        internal double AttackSpeed { get; set; }

        /// <summary>
        /// Constructor for Weapon object.
        /// </summary>
        internal Weapon(string WeaponName, int RequiredLevel, string Slot, double Damage, double AttackSpeed)
        {
            this.WeaponName = WeaponName;
            this.RequiredLevel = RequiredLevel;
            this.Slot = Slot;
            this.Damage = Damage;
            this.AttackSpeed = AttackSpeed;
        }

        /// <summary>
        /// Method to return a new Weapon object with corresponding properties.
        /// Throws Exception if Type doesn't match axe/bow/dagger/hammer/staff/sword/wand.
        /// </summary>
        /// <param name="Weapon"></param>
        /// <returns>Weapon</returns>
        internal static Weapon? GetWeapon(string weapon)
        {
            try
            {
                return weapon switch
                {
                    "axe" => new Weapon("Axe", REQUIRED_LEVEL_AXE, WEAPON_SLOT, DAMAGE_AXE, ATTACKSPEED_AXE),
                    "bow" => new Weapon("Bow", REQUIRED_LEVEL_BOW, WEAPON_SLOT, DAMAGE_BOW, ATTACKSPEED_BOW),
                    "dagger" => new Weapon("Dagger", REQUIRED_LEVEL_DAGGER, WEAPON_SLOT, DAMAGE_DAGGER, ATTACKSPEED_DAGGER),
                    "hammer" => new Weapon("Hammer", REQUIRED_LEVEL_HAMMER, WEAPON_SLOT, DAMAGE_HAMMER, ATTACKSPEED_HAMMER),
                    "staff" => new Weapon("Staff", REQUIRED_LEVEL_STAFF, WEAPON_SLOT, DAMAGE_STAFF, ATTACKSPEED_STAFF),
                    "sword" => new Weapon("Sword", REQUIRED_LEVEL_SWORD, WEAPON_SLOT, DAMAGE_SWORD, ATTACKSPEED_SWORD),
                    "wand" => new Weapon("Wand", REQUIRED_LEVEL_WAND, WEAPON_SLOT, DAMAGE_WAND, ATTACKSPEED_WAND),
                    _ => throw new Exception(),
                };
            }
            catch (Exception)
            {
                Console.WriteLine("Wrong input! Try using only lower case chars.");
                return null;
            }
        }
    }
}
