﻿using System.Text;
using rpg_console_app.Display;

namespace rpg_console_app
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("RPG CONSOLE APPLICATION");
            Console.WriteLine("");
            Warrior W = new Warrior();
            Console.WriteLine("");
            StringBuilder SB = new StringBuilder(25);

            Console.WriteLine("TEST 7");
            DisplayHero.DisplayHeroStats(SB, W);
            SB.Clear();

            Console.WriteLine("TEST 8");
            W.EquipWeapon("axe");
            DisplayHero.DisplayHeroStats(SB, W);
            SB.Clear();

            Console.WriteLine("TEST 9");
            W.EquipWeapon("axe");
            W.EquipArmour("body", "plate");
            DisplayHero.DisplayHeroStats(SB, W);
            SB.Clear();

            Console.WriteLine("TEST 1");
            W.EquipWeapon("sword");

            DisplayHero.DisplayHeroStats(SB, W);
            SB.Clear();

            Console.WriteLine("TEST 2");
            W.EquipArmour("head", "mail");
            DisplayHero.DisplayHeroStats(SB, W);
            SB.Clear();

            Console.WriteLine("TEST 3");
            W.EquipWeapon("bow");
            DisplayHero.DisplayHeroStats(SB, W);
            SB.Clear();

            Console.WriteLine("TEST 4");
            W.EquipArmour("leg", "cloth");
            DisplayHero.DisplayHeroStats(SB, W);
            SB.Clear();

            Console.WriteLine("TEST 5");
            W.EquipWeapon("axe");
            DisplayHero.DisplayHeroStats(SB, W);
            SB.Clear();

            Console.WriteLine("TEST 6");
            W.EquipArmour("leg", "plate");
            DisplayHero.DisplayHeroStats(SB, W);
            SB.Clear();

            Console.ReadKey();  // Stop execution
        }
    }
}